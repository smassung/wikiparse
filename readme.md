Simple way to extract Wikipedia article titles and text (still in wiki markup).

Used on [Wikipedia database
dump](http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2)

Usage:

```
./reader file.xml
```

Or to save to a file:

```
./reader file.xml > file.txt
```

You can also read from standard input, which is useful if decompressing on
the fly, e.g.:

```
bzcat file.xml.bz2 | ./reader - > file.txt
```
