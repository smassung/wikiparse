#include <iostream>
#include <algorithm>
#include <string.h>
#include <string>
#include <unistd.h>
#include <libxml/xmlreader.h>

bool blank(const std::string & str)
{
    for(auto & ch: str)
        if(ch != ' ')
            return false;
    return true;
}

void processNode(xmlTextReaderPtr reader, std::string & last_node)
{
    const char* n = (const char*) xmlTextReaderConstName(reader);
    const char* v = (const char*) xmlTextReaderConstValue(reader);
    std::string name = "";
    std::string value = "";

    if(n)
        name = n;
    if(v)
        value = v;

    if(last_node == "title" && name == "#text")
    {
        // remove spaces and newlines (if any?) from titles
        value.erase(value.find_last_not_of(" \n\r\t") + 1);
        std::remove(value.begin(), value.end(), '\n');
        std::replace(value.begin(), value.end(), ' ', '_');
        if(!blank(value))
            std::cout << value << " ";
    }
    else if(last_node == "text" && name == "#text")
    {
        std::replace(value.begin(), value.end(), '\n', ' ');
        if(!blank(value))
            std::cout << value << std::endl;
    }

    last_node = name;
}

void streamFile(const std::string & filename)
{
    xmlTextReaderPtr reader;

    if(filename == "-")
        reader = xmlReaderForFd(STDIN_FILENO, "", NULL, XML_PARSE_RECOVER);
    else
        reader = xmlReaderForFile(filename.c_str(), NULL, XML_PARSE_RECOVER);
    std::string last_node = "";

    while(xmlTextReaderRead(reader))
        processNode(reader, last_node);

    xmlFreeTextReader(reader);
}

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
        return 1;
    }

    streamFile(argv[1]);
    xmlCleanupParser();
    return 0;
}
