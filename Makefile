EXE = reader
CXX = clang++ -std=c++11
LINKER = clang++ -std=c++11
INCLUDE = -I/usr/include/libxml2
LIBS = -lxml2

all: $(EXE)

$(EXE): reader.cpp
	$(LINKER) reader.cpp -g $(INCLUDE) $(LIBS) -o $@

clean:
	rm $(EXE)
